package me.flyray.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author bolei
 */
@EnableDiscoveryClient
@EnableConfigServer
@SpringBootApplication
public class FlyrayConfigBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(FlyrayConfigBootstrap.class, args);
    }
}
