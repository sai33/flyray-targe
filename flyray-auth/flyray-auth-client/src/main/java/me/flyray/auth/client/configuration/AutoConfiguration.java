package me.flyray.auth.client.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import me.flyray.auth.common.config.ServiceAuthConfig;
import me.flyray.auth.common.config.UserAuthConfig;

/**
 * Created by ace on 2017/9/15.
 */
@Configuration
@ComponentScan({"me.flyray.auth.client","me.flyray.auth.common"})
public class AutoConfiguration {
    @Bean
    ServiceAuthConfig getServiceAuthConfig(){
        return new ServiceAuthConfig();
    }

    @Bean
    UserAuthConfig getUserAuthConfig(){
        return new UserAuthConfig();
    }

}
