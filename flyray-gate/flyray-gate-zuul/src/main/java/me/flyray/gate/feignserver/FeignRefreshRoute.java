package me.flyray.gate.feignserver;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.gate.Service.RefreshRouteService;

@RestController
@RequestMapping("feign/route")
public class FeignRefreshRoute {
	@Autowired
	private RefreshRouteService refreshRouteService;
	
	@RequestMapping(value = "refresh",method = RequestMethod.GET)
	public Map<String, Object> refreshRoute(){
		Map<String, Object> resp = refreshRouteService.refreshRoute();
		return resp;
	}
	
}
