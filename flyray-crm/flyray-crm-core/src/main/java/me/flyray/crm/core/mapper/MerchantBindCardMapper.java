package me.flyray.crm.core.mapper;


import me.flyray.crm.core.entity.MerchantBindCard;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author admin
 * @email ${email}
 * @date 2019-01-07 17:41:33
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantBindCardMapper extends Mapper<MerchantBindCard> {
	
}
