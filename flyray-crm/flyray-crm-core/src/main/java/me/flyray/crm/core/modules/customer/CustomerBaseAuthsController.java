package me.flyray.crm.core.modules.customer;

import me.flyray.crm.core.entity.CustomerBaseAuth;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.customer.CustomerBaseAuthBiz;

@RestController
@RequestMapping("customerBaseAuths")
public class CustomerBaseAuthsController extends BaseController<CustomerBaseAuthBiz, CustomerBaseAuth> {

}