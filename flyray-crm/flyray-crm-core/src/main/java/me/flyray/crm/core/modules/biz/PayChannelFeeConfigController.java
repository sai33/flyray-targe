package me.flyray.crm.core.modules.biz;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.PayChannelFeeConfigBiz;
import me.flyray.crm.core.entity.PayChannelFeeConfig;

import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

import me.flyray.crm.facade.request.PayChannelFeeConfigRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("payChannelFeeConfig")
public class PayChannelFeeConfigController extends BaseController<PayChannelFeeConfigBiz, PayChannelFeeConfig> {
	@Autowired
	private PayChannelFeeConfigBiz payChannelFeeConfigBiz;
	/**
	 * 添加
	 */
	@ApiOperation("费率配置新增")
	@RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody PayChannelFeeConfigRequest request){
		Map<String, Object> result = payChannelFeeConfigBiz.addPayChannelFee(request);
		return result;
	}
	/**
	 * 删除
	 */
	@ApiOperation("费率配置删除")
	@RequestMapping(value = "/delete/{feeCode}",method = RequestMethod.DELETE)
    @ResponseBody
	public Map<String, Object> delete(@PathVariable String feeCode){
		Map<String, Object> result = payChannelFeeConfigBiz.deletePayChannelFee(feeCode);
		return result;
	}
	/**
	 * 查询单个
	 */
	@ApiOperation("查询单个详情")
	@RequestMapping(value = "/queryInfo/{feeCode}",method = RequestMethod.GET)
    @ResponseBody
	public PayChannelFeeConfig queryInfo(@PathVariable String feeCode){
		return payChannelFeeConfigBiz.queryInfo(feeCode);
	}
	/**
	 * 修改
	 */
	@ApiOperation("费率配置修改")
	@RequestMapping(value = "/update",method = RequestMethod.PUT)
    @ResponseBody
	public Map<String, Object> update(@RequestBody PayChannelFeeConfigRequest request){
		Map<String, Object> result = payChannelFeeConfigBiz.updatePayChannelFee(request);
		return result;
	}
	/**
	 * 查询列表
	 */
	@ApiOperation("查询费率列表")
	@RequestMapping(value = "/queryList",method = RequestMethod.POST)
    @ResponseBody
	public TableResultResponse<PayChannelFeeConfig> queryList(@RequestBody PayChannelFeeConfigRequest request){
        
		TableResultResponse<PayChannelFeeConfig> response =payChannelFeeConfigBiz.queryList(request);
		return response;
	}
	/**
	 * 查询全部不分页
	 */
	@ApiOperation("查询费率列表")
	@RequestMapping(value = "/queryAll",method = RequestMethod.GET)
    @ResponseBody
	public List<PayChannelFeeConfig> queryAll(){
        
		List<PayChannelFeeConfig> list = payChannelFeeConfigBiz.selectListAll();
		return list;
	}
}