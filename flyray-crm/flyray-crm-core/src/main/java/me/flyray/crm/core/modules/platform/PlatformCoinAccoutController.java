package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.biz.platform.PlatformCoinAccoutBiz;
import me.flyray.crm.core.entity.PlatformCoinAccout;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformCoinAccout")
public class PlatformCoinAccoutController extends BaseController<PlatformCoinAccoutBiz, PlatformCoinAccout> {

}