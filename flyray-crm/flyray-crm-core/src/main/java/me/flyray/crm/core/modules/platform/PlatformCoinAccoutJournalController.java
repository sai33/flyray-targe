package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.biz.platform.PlatformCoinAccoutJournalBiz;
import me.flyray.crm.core.entity.PlatformCoinAccoutJournal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformCoinAccoutJournal")
public class PlatformCoinAccoutJournalController extends BaseController<PlatformCoinAccoutJournalBiz, PlatformCoinAccoutJournal> {

}