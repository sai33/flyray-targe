package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("注册请求参数")
public class RegisterRequest implements Serializable {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;

	@ApiModelProperty("手机号")
	private String phone;

	@ApiModelProperty("用户名")
	private String username;

	@NotNull(message="密码不能为空")
	@ApiModelProperty("密码")
	private String password;
	

//	@NotNull(message="验证码不能为空")
	@ApiModelProperty("验证码")
	private String smsCode;

	/**
	 * 商户类型
	 */
	@ApiModelProperty("商户类型")
	private String merchantType;

	@ApiModelProperty("客户类型")
	private String customerType;

	@ApiModelProperty("设备ID")
	private String deviceId;
	
	/**
	 * 设备类型：0-android、1-ios、2-PC
	 */
	@ApiModelProperty("设备类型")
	private String deviceType;
	
	@ApiModelProperty("密码盐值")
	private String passwordSalt;

	@ApiModelProperty("用户头像")
	private String avatar;

	@ApiModelProperty("用户邮箱")
	private String email;

	@ApiModelProperty("用户性别")
	private String sex;

	@ApiModelProperty("用户姓名")
	private String realName;

	@ApiModelProperty("昵称")
	private String nickname;

	//备注
	@ApiModelProperty(value = "备注")
	private String remark;

	//客户等级
	@ApiModelProperty(value = "客户等级")
	private String personalLevel;

	@ApiModelProperty(value = "是否为客户经理 1：是，0：否")
	private Integer isOwner;

	// 归属人（存后台管理系统登录人员id）指谁发展的客户
	@ApiModelProperty(value = "归属人")
	private String ownerId;

	@ApiModelProperty(value = "供业务拓展使用自定义")
	private Integer roleType;

	private String ownerName;

	private String authenticationStatus;
}
