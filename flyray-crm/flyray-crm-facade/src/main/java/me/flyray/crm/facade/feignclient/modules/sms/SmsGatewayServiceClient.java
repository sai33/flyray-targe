package me.flyray.crm.facade.feignclient.modules.sms;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.DistributionInviteRequest;
import me.flyray.crm.facade.request.SendSMSRequest;
import me.flyray.crm.facade.request.ValidateSMSRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 22:33 2019/3/13
 * @Description: 短信网关
 */

@FeignClient(value = "flyray-crm-core")
public interface SmsGatewayServiceClient {

    /**
     *  发送短信并保存到Redis中
     * @param sendSMSRequest
     * @return
     */
    @RequestMapping(value = "feign/sms/sendCode",method = RequestMethod.POST)
    BaseApiResponse sendCode(@RequestBody @Valid SendSMSRequest sendSMSRequest);

    /**
     *  校验短信验证码
     * @param validateSMSRequest
     * @return
     */
    @RequestMapping(value = "feign/sms/validateSmsCode",method = RequestMethod.POST)
    BaseApiResponse validateSmsCode(@RequestBody @Valid ValidateSMSRequest validateSMSRequest);

}
