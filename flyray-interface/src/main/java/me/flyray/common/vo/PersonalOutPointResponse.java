package me.flyray.common.vo;

import lombok.Data;

import java.io.Serializable;


/**
 * 个人客户信息
 * @author centerroot
 * @time 创建时间:2018年8月14日下午2:47:21
 * @description
 */

@Data
public class PersonalOutPointResponse implements Serializable {
	
	//序号
    private Integer id;
    
	//个人客户编号
    private String personalId;

    //流水id
    private String journalId;

    //积分账户余额
    private String point;

	//历年积分账户余额
    private String hisPoint;

    //积分总账余额
    private String totalPoint;

}