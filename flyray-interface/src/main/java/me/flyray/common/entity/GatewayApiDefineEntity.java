package me.flyray.common.entity;

import lombok.Data;

import java.io.Serializable;


/**
 * 动态路由表
 * @author centerroot
 * @email ${email}
 * @date 2018-08-28 15:36:21
 */

@Data
public class GatewayApiDefineEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    private String id;
	
	    //
    private String path;
	
	    //
    private String serviceId;
	
	    //
    private String url;
	
	    //
    private Integer retryable;
	
	    //
    private Integer enabled;
	
	    //
    private Integer stripPrefix;
	
	    //
    private String apiName;

}
