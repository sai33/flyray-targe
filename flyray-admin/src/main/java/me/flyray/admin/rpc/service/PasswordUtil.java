package me.flyray.admin.rpc.service;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordUtil {
	
	public static final String SALT="HZMetro_DtDreamHJOJHDJ)*UKI&HBHJUM<BTXUDHGE*&^#*EHHJS";
	
	public static String getPassword(String password,Integer userId){
		password = DigestUtils.sha512Hex(password+"HZMetro_DtDream&(SJDJ^EHDJOO#KSDISDJF#*(OSJsisj82*AS");
		if (password.length()>=10) {
			return DigestUtils.sha512Hex(password.substring(0, 9)+SALT+password.substring(9)+userId);
		}else {
			return "";
		}
	}

	public static void main(String[] args) {
		System.out.println(PasswordUtil.getPassword("admin", 1));
	}
}
