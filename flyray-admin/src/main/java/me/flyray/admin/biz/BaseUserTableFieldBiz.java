package me.flyray.admin.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.admin.rest.BaseUserTableFieldRequest;
import me.flyray.admin.entity.BaseUserTableField;
import me.flyray.admin.mapper.BaseUserTableFieldMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 用户动态显示字段
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-30 15:31:30
 */
@Slf4j
@Service
public class BaseUserTableFieldBiz extends BaseBiz<BaseUserTableFieldMapper, BaseUserTableField> {
	
	
	/**
	 * 查询用户显示字段
	 * @author centerroot
	 * @time 创建时间:2018年8月30日下午3:45:28
	 * @param baseUserTableFieldRequest
	 * @return
	 */
	public Map<String, Object> queryObj(BaseUserTableFieldRequest baseUserTableFieldRequest){
		log.info("【查询用户显示字段】   请求参数：{}",EntityUtils.beanToMap(baseUserTableFieldRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		BaseUserTableField baseUserTableFieldReq = new BaseUserTableField();
		baseUserTableFieldReq.setPlatformId(baseUserTableFieldRequest.getPlatformId());
		baseUserTableFieldReq.setPageTag(baseUserTableFieldRequest.getPageTag());
		baseUserTableFieldReq.setUserId(baseUserTableFieldRequest.getUserId());
		
		List<BaseUserTableField> baseUserTableFieldList = mapper.select(baseUserTableFieldReq);
		if (null != baseUserTableFieldList && baseUserTableFieldList.size() > 0) {
			BaseUserTableField baseUserTableField = baseUserTableFieldList.get(0);
			respMap.put("baseUserTableField", baseUserTableField);
		}
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("message", ResponseCode.OK.getMessage());

		log.info("【查询用户显示字段】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加或更新用户显示字段
	 * @author centerroot
	 * @time 创建时间:2018年8月30日下午3:45:28
	 * @param baseUserTableFieldRequest
	 * @return
	 */
	public Map<String, Object> addOrUpdate(BaseUserTableFieldRequest baseUserTableFieldRequest){
		log.info("【添加或跟新用户显示字段】   请求参数：{}",EntityUtils.beanToMap(baseUserTableFieldRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		BaseUserTableField baseUserTableFieldReq = new BaseUserTableField();
		baseUserTableFieldReq.setPlatformId(baseUserTableFieldRequest.getPlatformId());
		baseUserTableFieldReq.setPageTag(baseUserTableFieldRequest.getPageTag());
		baseUserTableFieldReq.setUserId(baseUserTableFieldRequest.getUserId());
		
		List<BaseUserTableField> baseUserTableFieldList = mapper.select(baseUserTableFieldReq);
		if (null != baseUserTableFieldList && baseUserTableFieldList.size() > 0) {
			BaseUserTableField baseUserTableField = baseUserTableFieldList.get(0);
			baseUserTableField.setColumns(baseUserTableFieldRequest.getColumns());
			baseUserTableField.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			mapper.updateByPrimaryKeySelective(baseUserTableField);
			
		} else {
			baseUserTableFieldReq.setColumns(baseUserTableFieldRequest.getColumns());
			baseUserTableFieldReq.setCreateTime(new Timestamp(System.currentTimeMillis()));
			mapper.insertSelective(baseUserTableFieldReq);
		}
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("message", ResponseCode.OK.getMessage());

		log.info("【添加或跟新用户显示字段】   响应参数：{}", respMap);
		return respMap;
	}
}