package me.flyray.admin.biz;

import me.flyray.admin.entity.BaseIpList;
import me.flyray.admin.mapper.BaseIpListMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;


/**
 * 白名单
 *
 * @author mu
 * @email ${email}
 * @date 2018-08-28 14:30:04
 */
@Service
public class BaseIpListBiz extends BaseBiz<BaseIpListMapper, BaseIpList> {
}