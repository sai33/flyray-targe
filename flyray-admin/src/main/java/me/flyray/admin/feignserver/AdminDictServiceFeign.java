package me.flyray.admin.feignserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.vo.DictResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.admin.biz.DictBiz;
import me.flyray.admin.entity.Dict;
import me.flyray.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/dict")
public class AdminDictServiceFeign {
	@Autowired
	private DictBiz dictBiz;
	
	/**
	 * 根据类型查询字典表数据
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "selectByType",method = RequestMethod.POST)
    @ResponseBody
	public BaseApiResponse<List<DictResponse>> selectByType(@RequestBody Map<String, Object> param) {
		String type = (String) param.get("type");
		Dict dict = new Dict();
		dict.setType(type);
		List<Dict> dictList = dictBiz.selectList(dict);
		List<DictResponse> dictRes = new ArrayList<>();
		for (Dict dict1 : dictList){
			DictResponse dictResponse = new DictResponse();
			BeanUtils.copyProperties(dict1,dictResponse);
			dictRes.add(dictResponse);
		}
		return BaseApiResponse.newSuccess(dictRes);
	}
}
