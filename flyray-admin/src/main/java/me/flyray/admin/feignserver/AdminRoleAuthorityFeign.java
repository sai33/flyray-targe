package me.flyray.admin.feignserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.admin.biz.ResourceAuthorityBiz;
import me.flyray.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/authority")
public class AdminRoleAuthorityFeign {
	
	@Autowired
	private ResourceAuthorityBiz resourceAuthorityBiz;
	
	@RequestMapping(value = "platform",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformAuthority(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = resourceAuthorityBiz.copyResourceAuthority(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", ResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
		return result;
	}

}
