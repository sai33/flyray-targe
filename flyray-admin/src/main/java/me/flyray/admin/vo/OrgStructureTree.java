package me.flyray.admin.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: bolei
 * @date: 16:59 2019/3/15
 * @Description: 组织架构树
 */

@Data
public class OrgStructureTree extends OrgStructureNode{

    private List<OrgStructureNode> children;

}
